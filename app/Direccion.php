<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{	
	use SoftDeletes;
	
    protected $table = 'direcciones';
    protected $fillable = [
    						'persona_id',
							'campo_1',
							'campo_2',
							'campo_3',
							'campo_4',
							'ciudad',
							'estado',
							'municipio',
							'parroquia',
							'posicion',
						];
	public function persona()
    {
        return $this->belongsTo('App\Persona');
    }
}
