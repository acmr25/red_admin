<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
	use SoftDeletes;
	
    protected $table = 'personas';
    protected $fillable = [
    						'user_id'
    						'nombre',
							'apellido',
							'tipo_persona',
							'cedula',
							'fecha_nacimiento',
						];

	public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function contactos()
    {
        return $this->hasMany('App\Contacto','persona_id');
    }

    public function direcciones()
    {
        return $this->hasMany('App\Direccion','persona_id');
    }
}
 				
