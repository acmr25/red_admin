<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
	use SoftDeletes;
	
    protected $table = 'contactos';
    protected $fillable = [
    						'persona_id',
							'email',
							'telefono',
							'predeterminado',
						];

	public function persona()
    {
        return $this->belongsTo('App\Persona');
    }

}
