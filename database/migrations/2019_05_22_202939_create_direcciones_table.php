<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direcciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('campo_1');
            $table->string('campo_2');
            $table->string('campo_3');
            $table->string('campo_4');
            $table->string('estado');
            $table->string('municipio');
            $table->string('parroquia');
            $table->string('ciudad');
            $table->geometry('posicion');
            $table->integer('persona_id')->unsigned();
            $table->foreign('persona_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones');
    }
}
